package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.CompraTvExitoPage;

import net.thucydides.core.annotations.Step;

public class CompraTvExitoSteps {
	CompraTvExitoPage compraTvExitoPage;

	@Step
	public void AbrirPagina() {
		compraTvExitoPage.open();
		
	}

	@Step
	public void Busquedad(String articulo) throws InterruptedException {
		compraTvExitoPage.BusquedadPalabra(articulo);
		
	}
	
	@Step
	public void ImprimoInformacionTVS() {
		compraTvExitoPage.ImprimoInformacionTVS();
		
	}

	@Step
	public void SeleccionoTVs() {
		compraTvExitoPage.ClickTelevisor();
		
	}

	@Step
	public void RealizarCompra() throws InterruptedException {
		compraTvExitoPage.RealizarCompra();
		
	}
	
	@Step
	public void VerificarCompra() {
		compraTvExitoPage.VerificarCompra();
	}

}
