package com.choucair.formacion.pageobjects;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.exito.com/")
public class CompraTvExitoPage extends PageObject{
	public WebElement tv;

	// LabelBusqueda
	@FindBy(id = "tbxSearch")
	public WebElementFacade txtBuscar;

	// Botón busqueda
	@FindBy(id = "btnSearch")
	private WebElementFacade btnBuscar;
	

	// Link 49 pulgadas del TV
	@FindBy(xpath = "//*[@id ='filterBy']//*[@class ='mobil-link'][contains(text(),'de 44 a 49')]")
	public WebElementFacade lnkPulgadasTv;


	// Check marca del TV
	@FindBy(xpath = "//*[@id ='filterBy']//*[contains(text(),'LG')]/input")
	public WebElementFacade chkMarcaTV;

	// Link resolución
	@FindBy(xpath = "//*[@id ='filterBy']//*[text() = 'UHD-4K ']")
	public WebElementFacade lnkResolucion;
	
	// Tabla de productos
	@FindBy(xpath = "//*[@class = 'row product-list']")
	public java.util.List<WebElement> TblProductos;
	
	//Botón agregar Producto
	@FindBy(xpath="//*[contains(@class,'form-group col-addtocart')]/button")
	public WebElementFacade btnAgregar;
	
	//linkCArro
	@FindBy(xpath="//*[contains(@class, 'header-shopping-cart')]/a/span")
	public WebElementFacade linkCarrito;
	
	//Row Productos Agregados
	@FindBy(xpath = "//*[contains(@id, 'summary-page')]//*[contains(text(), 'en tu carrito')]")
	public WebElementFacade rowProduct;
	//Cerra Popup
	@FindBy(xpath="//*[contains(@id,'bunting_lightbox-710')]/a")
	public WebElementFacade cerrarPopup;
	

	public void BusquedadPalabra(String Articulo) throws InterruptedException {
		txtBuscar.sendKeys(Articulo);
		btnBuscar.click();
		JavascriptExecutor j = (JavascriptExecutor) this.getDriver();
		j.executeScript("window.scrollBy(0,500)", "");
		lnkPulgadasTv.click();
		j.executeScript("window.scrollBy(0,500)", "");
		chkMarcaTV.click();
		j.executeScript("window.scrollBy(0,500)", "");
		lnkResolucion.click();
		Thread.sleep(3000);	
	}

	public void ImprimoInformacionTVS() {		
		int cont=0;
		for(WebElement trElement : TblProductos) {
			java.util.List<WebElement> tblColumna = trElement.findElements(
					By.xpath("div//*[contains(@class, 'row')]/a"));
			System.out.println(trElement.getText());	
			for (WebElement tdElement2 : tblColumna) {
				
				if (cont == 0) {
					tv = tdElement2;
				}
				cont++;
		}
		
		}
	}


	public void ClickTelevisor() {
		tv.click();
	}

	public void RealizarCompra() throws InterruptedException {
		btnAgregar.click();
		Thread.sleep(5000);
		
		if (cerrarPopup.isCurrentlyVisible())
			cerrarPopup.click();

		Thread.sleep(3000);
		linkCarrito.click();
		Thread.sleep(3000);
		
	}

	public void VerificarCompra() {
		JavascriptExecutor j = (JavascriptExecutor) this.getDriver();
		j.executeScript("window.scrollBy(0,500)", "");
		MatcherAssert.assertThat("No se encontraron productos", rowProduct.isDisplayed());
		
	}
}
